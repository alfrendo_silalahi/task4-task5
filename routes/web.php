<?php

use App\Http\Controllers\ExcelController;
use App\Http\Controllers\PDFController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\CompanyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [GeneralController::class, 'showHome']);

Route::get('/home', [GeneralController::class, 'showHome']);

Route::get('/data-pekerja', [EmployeeController::class, 'showDataPekerja']);

Route::get('/tambah', [EmployeeController::class, 'tambahData']);

Route::post('/tambah', [EmployeeController::class, 'simpanData']);

Route::get('/edit/{slug}', [EmployeeController::class, 'editData']);

Route::post('/edit/{slug}', [EmployeeController::class, 'simpanEditData']);

Route::get('/hapus/{slug}', [EmployeeController::class, 'hapusData']);

// Company
Route::get('/data-company', [CompanyController::class, 'showDataCompany']);

Route::get('/tambahDataCompany', [CompanyController::class, 'tambahData']);

Route::post('/tambahDataCompany', [CompanyController::class, 'simpanData']);

Route::get('/hapusDataCompany/{slug}', [CompanyController::class, 'hapusData']);

Route::get('/editDataCompany/{slug}', [CompanyController::class, 'editData']);

Route::post('/editDataCompany/{slug}', [CompanyController::class, 'simpanEditData']);

Route::get('generate-pdf', [PDFController::class, 'generatePDF']);

Route::get('file-export', [ExcelController::class, 'fileExport']);
